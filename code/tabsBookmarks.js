
function test() {
  console.log("Wow! Too many tabs!")
}

const HIDDEN_BOOKMARKS = "Recent Tabs"
const BOOKMARKS_BAR_INDEX = "1"

const btnTabsBookmark = document.getElementById("btnTabsBookmark");
btnTabsBookmark.addEventListener('click', test);

